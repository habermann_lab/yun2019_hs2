{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# (2) Differential expression analysis\n",
    "---\n",
    "Michaël Pierrelée, Aix Marseille Univ, CNRS, IBDM, UMR7288, FRANCE - michael.pierrelee@univ-amu.fr\n",
    "\n",
    "*Apache License 2.0*\n",
    "\n",
    "---\n",
    "\n",
    "Differential expression analysis is performed first using a standard DESeq2 workflow (DESeq normalization ([Anders 2010](https://doi.org/10.1186/gb-2010-11-10-r106)) -> GLM statistical test)  and second using SVCD normalization ([Roca 2017](https://doi.org/10.1038/srep42460)) followed by DESeq2 GLM test.\n",
    "\n",
    "Raw counts are rounded to the closest integer and the genes without one count across samples are filtered out.\n",
    "\n",
    "SVCD requires to exclude lowly expressed genes in order to calculate normalization factors from genes identified as non-varying by the algorithm. The count threshold is computed to remove 25% of genes with the lowest gene expression mean. For this dataset, it gave a cutoff at around 60 counts. As needed by SVCD, the filtered matrix is converted into log2-base before normalization and the offsets are then converted back into decimal base before to be assigned to the DESeqDataSet object.\n",
    "\n",
    "For pairwise comparisons, design matrix is made by grouping replicates from each condition into one individual group. To compute the interaction term, e.g. what is the difference between the dysregulations at the E and S phases, the design is `~medium + phase + medium:phase` by assuming that the medium is the first parameter explaining the variations.\n",
    "\n",
    "Finally, the exported results have the average SVCD-normalized counts between replicates for each condition.\n",
    "\n",
    "*NB: SVCD normalization does not return exact offsets, so the results are sligty different at each run. To avoid any problem, the result export is commented.*\n",
    "\n",
    "## Input\n",
    "* `data/Trinity_genes.gene.counts.matrix`: raw counts from read quantification per gene by RSEM.\n",
    "* `data/sample_files.tsv`: description of the experimental design.\n",
    "\n",
    "## Output\n",
    "\n",
    "Results from DESeq2 (within `data/dea_results`):\n",
    "* `deseq-ME_vs_FE.csv` and `deseq-MS_vs_FS.csv`: samples from marine water M vs. fresh water F at exponential phase (E) and stationary phase (S) (resp.), using DESeq2 standard workflow;\n",
    "* `deseq2-size_factors.csv`: size factors from DESeq2 normalization step;\n",
    "* `deseq2-normalized_counts.csv`: normalized counts after DESeq2 normalization step;\n",
    "* `svcd-ME_vs_FE.csv` and `svcd-MS_vs_FS.csv`: same as for DESeq2, using SVCD replacing DESeq2 normalization step;\n",
    "* `svcd-interaction_term.csv`: test if medium effect is different across phase, using SVCD workflow;\n",
    "* `svcd-normalized_counts.csv` and `svcd-mean_norm_counts.csv`: normalized counts and their means in each condition, from SVCD workflow;\n",
    "* `svcd-offsets.csv`: normalization factors from SVCD normalization step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "suppressMessages(library( \"DESeq2\" ))\n",
    "suppressMessages(library( \"cdnormbio\" ))\n",
    "library(\"BiocParallel\")\n",
    "register(MulticoreParam(16))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " [1] \"TN1610R1293.1\" \"TN1610R1293\"   \"TN1610R1294\"   \"TN1610R1295\"  \n",
      " [5] \"TN1610R1296\"   \"TN1610R1297\"   \"TN1610R1299.1\" \"TN1610R1299.2\"\n",
      " [9] \"TN1610R1299\"   \"TN1610R1301\"   \"TN1610R1302\"   \"TN1610R1303\"  \n",
      " [1] TN1610R1293-1 TN1610R1293   TN1610R1294   TN1610R1295   TN1610R1296  \n",
      " [6] TN1610R1297   TN1610R1299-1 TN1610R1299-2 TN1610R1299   TN1610R1301  \n",
      "[11] TN1610R1302   TN1610R1303  \n",
      "12 Levels: TN1610R1293 TN1610R1293-1 TN1610R1294 TN1610R1295 ... TN1610R1303\n",
      "[1] \"number of genes: 57640\"\n"
     ]
    }
   ],
   "source": [
    "# table of raw counts\n",
    "rawCounts = read.csv( \"../data/Trinity_genes.gene.counts.matrix\", header=T, row.names=1, sep = '\\t'  )\n",
    "print( colnames(rawCounts) )\n",
    "\n",
    "# sample files\n",
    "sample_files = read.csv( \"../data/sample_files.tsv\", header=F, sep = '\\t' )\n",
    "print( sample_files$V1 )\n",
    "\n",
    "# rename column names\n",
    "colnames( rawCounts ) = sample_files$V3\n",
    "\n",
    "# colData\n",
    "medium = sample_files$V4\n",
    "phase = sample_files$V5\n",
    "ind = paste( medium, phase, sep='' )\n",
    "\n",
    "chloInfoTable = data.frame( replicates = colnames( rawCounts ), ind = ind, medium = medium, phase = phase )\n",
    "\n",
    "print( paste('number of genes:', dim(rawCounts)[1]) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"number of genes: 52770\"\n",
      "[1] \"number of filtered genes: 4870\"\n"
     ]
    }
   ],
   "source": [
    "# round\n",
    "chloCountTable = round( rawCounts, 0 )\n",
    "# remove integers with null expression\n",
    "chloCountTable = chloCountTable[ rowSums( chloCountTable ) > 1, ]\n",
    "# convert to integers\n",
    "chloCountTable = as.data.frame( lapply( chloCountTable, as.integer ), row.names = row.names(chloCountTable) )\n",
    "\n",
    "print( paste('number of genes:', dim(chloCountTable)[1]) )\n",
    "print( paste('number of filtered genes:', dim(rawCounts)[1] - dim(chloCountTable)[1]) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## DEA by DESeq2\n",
    "Normalization and statistical tests were both performed by DESeq2 using standard worflow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# normalization\n",
    "dds = DESeqDataSetFromMatrix( countData = chloCountTable, colData = chloInfoTable, design = ~ind )\n",
    "dds = DESeq( dds, quiet = TRUE, parallel = TRUE )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```R\n",
    "# export\n",
    "write.csv( dds$sizeFactor, '../data/dea_results/deseq2-size_factors.csv' )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# individual comparisons\n",
    "resE = results( dds, contrast = c( 'ind', 'ME', 'FE' ), parallel = TRUE )\n",
    "resS = results( dds, contrast = c( 'ind', 'MS', 'FS' ), parallel = TRUE )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "out of 52770 with nonzero total read count\n",
      "adjusted p-value < 0.1\n",
      "LFC > 0 (up)       : 4266, 8.1%\n",
      "LFC < 0 (down)     : 5086, 9.6%\n",
      "outliers [1]       : 10257, 19%\n",
      "low counts [2]     : 13237, 25%\n",
      "(mean count < 7)\n",
      "[1] see 'cooksCutoff' argument of ?results\n",
      "[2] see 'independentFiltering' argument of ?results\n",
      "\n"
     ]
    }
   ],
   "source": [
    "summary(resE)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "out of 52770 with nonzero total read count\n",
      "adjusted p-value < 0.1\n",
      "LFC > 0 (up)       : 3082, 5.8%\n",
      "LFC < 0 (down)     : 8327, 16%\n",
      "outliers [1]       : 10257, 19%\n",
      "low counts [2]     : 13237, 25%\n",
      "(mean count < 7)\n",
      "[1] see 'cooksCutoff' argument of ?results\n",
      "[2] see 'independentFiltering' argument of ?results\n",
      "\n"
     ]
    }
   ],
   "source": [
    "summary(resS)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```R\n",
    "# export\n",
    "write.csv( resE, '../data/dea_results/deseq2-ME_vs_FE.csv' )\n",
    "write.csv( resS, '../data/dea_results/deseq2-MS_vs_FS.csv' )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mean of counts for each condition\n",
    "deseq_norm_counts = counts(dds, normalized=TRUE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```R\n",
    "# export\n",
    "write.csv( deseq_norm_counts, '../data/dea_results/deseq2-normalized_counts.csv' )\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Normalization by SVCD and tests by DESeq2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"count threshold: 5.91666666666667\"\n"
     ]
    }
   ],
   "source": [
    "# raw data filtering\n",
    "cct = chloCountTable[ apply(chloCountTable, 1, FUN=min) >= 1 & rowMeans(chloCountTable) >= quantile(rowMeans(chloCountTable), .25), ]\n",
    "print( paste( 'count threshold:', quantile( rowMeans(chloCountTable), .25) ) )\n",
    "\n",
    "# normalization function\n",
    "res.svcd = normalize.svcd( as.matrix(log2( cct )), chloInfoTable$ind )\n",
    "offsets = res.svcd$offset\n",
    "\n",
    "# individual comparisons\n",
    "dds = DESeqDataSetFromMatrix( countData = chloCountTable, colData = chloInfoTable, design = ~ind )\n",
    "sizeFactors( dds ) = 2^offsets\n",
    "dds = DESeq( dds, quiet = TRUE, parallel = TRUE )\n",
    "\n",
    "resE = results( dds, contrast = c( 'ind', 'ME', 'FE' ), parallel = TRUE )\n",
    "resS = results( dds, contrast = c( 'ind', 'MS', 'FS' ), parallel = TRUE )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "out of 52770 with nonzero total read count\n",
      "adjusted p-value < 0.1\n",
      "LFC > 0 (up)       : 4266, 8.1%\n",
      "LFC < 0 (down)     : 5086, 9.6%\n",
      "outliers [1]       : 10257, 19%\n",
      "low counts [2]     : 13237, 25%\n",
      "(mean count < 7)\n",
      "[1] see 'cooksCutoff' argument of ?results\n",
      "[2] see 'independentFiltering' argument of ?results\n",
      "\n"
     ]
    }
   ],
   "source": [
    "summary(resE)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "out of 52770 with nonzero total read count\n",
      "adjusted p-value < 0.1\n",
      "LFC > 0 (up)       : 3082, 5.8%\n",
      "LFC < 0 (down)     : 8327, 16%\n",
      "outliers [1]       : 10257, 19%\n",
      "low counts [2]     : 13237, 25%\n",
      "(mean count < 7)\n",
      "[1] see 'cooksCutoff' argument of ?results\n",
      "[2] see 'independentFiltering' argument of ?results\n",
      "\n"
     ]
    }
   ],
   "source": [
    "summary(resS)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA0gAAANICAMAAADKOT/pAAACbVBMVEUAAAABAQECAgIDAwME\nBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUW\nFhYYGBgZGRkaGhodHR0gICAiIiIjIyMkJCQlJSUmJiYnJycoKCgqKiorKyssLCwtLS0wMDAy\nMjIzMzM0NDQ1NTU3Nzc5OTk6Ojo9PT0+Pj5AQEBERERFRUVGRkZHR0dISEhJSUlLS0tMTExN\nTU1RUVFTU1NUVFRVVVVXV1dZWVlaWlpbW1teXl5gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2do\naGhpaWlra2tsbGxubm5vb29wcHBxcXFycnJzc3N0dHR1dXV3d3d4eHh5eXl6enp7e3t8fHx9\nfX1+fn6BgYGDg4OEhISGhoaHh4eIiIiJiYmKioqLi4uMjIyNjY2Ojo6QkJCSkpKTk5OUlJSV\nlZWWlpaXl5eYmJiZmZmbm5ucnJyenp6goKCioqKjo6OlpaWmpqanp6eoqKipqamqqqqsrKyt\nra2xsbGysrK0tLS1tbW2tra3t7e4uLi5ubm6urq7u7u8vLy9vb2+vr6/v7/AwMDBwcHCwsLD\nw8PExMTFxcXGxsbHx8fIyMjJycnKysrLy8vMzMzNzc3Ozs7Pz8/Q0NDR0dHS0tLT09PU1NTV\n1dXW1tbX19fZ2dna2trb29vc3Nzd3d3e3t7f39/g4ODh4eHi4uLj4+Pk5OTl5eXm5ubn5+fo\n6Ojq6urr6+vs7Ozt7e3u7u7v7+/w8PDx8fHy8vLz8/P09PT19fX29vb39/f4+Pj5+fn6+vr7\n+/v8/Pz9/f3+/v7///+rO7taAAAACXBIWXMAABJ0AAASdAHeZh94AAAgAElEQVR4nO3di7sd\nV1nH8d0ScmsIhlYL0tKCVQg3aa2AQGuLEbGClJumXBSKEcVovUTE4qUtpASI3BQQaWhT6w1p\niqBAgylULrZNe/4mwb3Xu08nKzOz3vWumXfWfD/Po+znnL57rTMzv5yz5/yys9gAkG0x9gaA\nGhAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAA\nQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAk\nwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIM\nECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABB\nAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTA\nAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQ\nJMAAQQIMECTAwABBOn4MmJTj6Vd5+SDdsQAm5o7ky7x8kD6zeKj4GoChhxafSZ4hSEADQQIM\nECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABB\nAgwQJMAAQQIMTD1Ij5wSX18//F7H1Lfkvzx5Uh5+q2PorqPBkUPy8OjXem60Kuvjd//9vY+f\nbqX1+1196A55eF/H1IOyqW9+Ux4+WGJ/YupB2h99Z6SL2oeOxd9P6Vj71HP3BLvOkYd73tk+\n9IGrxU/8nDy8oX3oS1deEVx2mTy88kvtU8+XPe3est7fu9qH3ho9Epe0D+mO38/vDS57ijzc\n+6ftQzdEV3pGif1dGh16W/uQmHqQvnuv2PleeXh/x9R98l++4hXysOuPubXD5/X+T49cJ7a8\nWB7e2D506t0Hgr175eG7T7VPffaW4IZt8vCWr7QPPbD+E3/HQXl4ouOrUh2/v5CvZN9WeXjg\nC+1Dj61/zNj5V/LwfzuG7pav5GUvk4d3P9Y+da/8lwd3rI9Kx0EXUw/SJucdVqy0b59iKCFI\nm7C/oMb9ESTFUI0XAvtbI0jpuBAE+wtmHqQLPq5Y6bWvVQwde45iaMAL4chuxdDGnqOKIdXx\nu32XYmjjxz+rGHrNaxRDH92TPlNRkE5qVvrudzVTKr/1dcXQ5z6nGHoo/f3cf+DLjyqGVMfv\n27cohjY029t4UHPT+9ET6TMVBQkYD0ECDFQUpI5fLsSdPq0YevhuzVJVeuSRwZb6asfvgaIe\nVf1AqFBRkJ76t4qVrr9eMfThJyuGNn7zPxVD73ufYujO5yuGNp7+94oh1fH7t2sVQxtP0dwM\necMbFEOfuzh9pqIg+b49yv7WatwfQVIM1XghsL81gpSOC0Gwv4AgpeNCEOwvmHmQaDYER3Yr\nhmg2CJoN6Wg2CJoNstKJ9JmKggSMhyABBioKEs2GEdBsWKkoSDQbApoNgmZD+pDv26Psb63G\n/REkxVCNFwL7WyNI6bgQBPsLCFI6LgTB/oKZB4lmQ3Bkt2KIZoOg2ZCOZoOg2SArnUifqShI\nwHgIEmCgoiDRbBgBzYaVioJEsyGg2SBoNqQP+b49yv7WatwfQVIM1XghsL81gpSOC0Gwv4Ag\npeNCEOwvmHmQaDYER3Yrhmg2CJoN6Wg2CJoNstKJ9JmKggSMhyABBioKEs2GEdBsWKkoSDQb\nApoNovpmw+IHzv5Zbn8H7E/43t9IQVosWpNEkAL2J3zvb7QgLf/vLAhSwP6E7/2NE6Rlhs6e\nJIIUsD/he38VBYlmQ3Bkt2KIZoOYTrMhFqSH//yQ+DWaDSs0GwTNhsgzRIL0H8+8SPzo4n+y\n1wAG5CdIm/0JQcK0VBQkmg0joNmwUlGQaDYENBtE7c2Gjt8jqYLk+/Yo+1urcX8+mw0EKWB/\nwvf+fHbtCFLA/oTv/flsfxOkgP0J3/urKEg0G4IjuxVDNBvEdJoNXVRBotkQ0GwQlTcbuvAL\nWUwMQQIMVBQkmg0joNmwUlGQaDYENBtE7c2GDtz+Dtif8L0/gqQYqvFCYH9rBCkdF4JgfwFB\nSseFINhfMPMg0WwIjuxWDNFsEDQb0tFsEDQbZKUT6TMVBQkYD0ECDFQUpKk3Gz51S2+fkqGD\nF/X2zH8qsGmaDSsVBalIs+G+2HX8tp2xj97WkeSuZsOTLuwbiQufJEP7XnCor20dNztoNgQ0\nG9J13RV71dY9Z9q9JfLBPeccydtf//1vuj2bcFfPbv1NuP29QpDyPh9d//iPRYO2K/bRd0Xm\nuxCk3ghS+pCfIB3eFvvR78b3Rz54+b7IfBeC1BtBSh9yFKT+J2IfQerL9/4qClKRZsOMgkSz\nIaDZkK7rN/MzChLNBlnpRPpMRUEqYkZBQg6C1I4goZeKglSk2UCQ2k292fBfR3v72MNtTzTJ\nIP3r8/ZGPPGZsY/+kUy98eqISy6JffSNMjSjIM2y2fBLsV+4x53z4bYnmmSQDm89EPEL74h8\ncO+mC/Wq/We65prIB6/KvFCnGSQ/t5cffE/s/G7dF/vo8bz9mR2/aQbJ+YXqfX+6z0cVCdLt\n58Z+tthzWeSD5/9i3v4IUk8Eqffz+wmS7vgV+fwmBGmFIOV9PspRkC5e9HZxbL4LQVohSO2f\n99NsUB6/GyO32m69NfLBGwscP4LU/vwzCpKfZsMUjx9Ban/+GQXJjykeP4LU/vwEaQRTPH4E\nqf35CVK7Is2GKR4/gtT+/DMKUlez4a5Yb+blL4999K72Z+pqNkzx+BGk9uevMkj3XBJ7S5Vz\no2++8h6Zf3Lsr8pv3x754K6Of/amzO1vgnQmgtQqe3/bYu8y9MabIh98Qeb+bop1FJ4RbS48\n718i810IUiuC1GpK+4u9Xdjv/2r724V5P34xBKn9+d1fqOzvzPkuBGnF+4Fmf7H5LtPZXwxB\nan/+Ki8E9heb70KQVuq8EC7v/YbhOyZzoXrfX8w0g7S99xtev3B9oHYePNbTwZ0y5P5C6F95\n3jKZC/Xw9thf4YvaS5DadAVpS//LZ32gtvUf2iZDBClzf6rjpzq/O2J/Azrqqh15+4shSDG5\nQdp6RV9P2/Qd83V9v82+btN3zP5fVO1B0p3fvb2/+W0nSEsDBmnA/b2w90+52zcFPfaOL1FP\nzw36pdf1tW2U49d/aEuFQdrR+xX25bnf+jUHmqBnHj/V+SVIZ3J01051IRCkvOOnOr+6m0ma\n/cUQpPbnH/JCuKHvH8M35N5VrDJIuvN7Ze83iNxBkJZ0B/ppve8bbJ3Oi/nzTvV1be5rpFfc\n29fOyRy/GILU/vzub3+zvzPnuxCkFe8Hmv3F5rtMZ38xBKn9+au8ENhfbL4LQVrhQojMd2F/\nPZ+fILU/f5UXAvuLzXchSCtcCJH5Luyv5/MTpPbnr/JCYH+x+S4EaYULITLfhf31fH6C1P78\nVV4I7C8234UgrXAhROa7sL+ez0+Q2p9/31N7V4SecPsY+6vxQj28LdYw/O2/bG9/6/YXe7uw\nm94UKz5tI0hLqgP9+d5/7+vAux+Q+Z29u2zXEKQzn/8LF0T/MeTYO73ueUfe/n439u6yF54b\n++gl97Q9J0FSPP+x57R//m/6V54XvyJTu2MXyo4dsbcE3r3++n4q8veprv/Z2N+yeuJkglTm\n82tdX3/C8REESfH8XZ9/7Hjfvxlz7Ni3ZerTsZ9nXvnK2Ec/LUPvj/2Q+ewnxD76khN997/J\nhIK0K/be5RdcEPuOY/cvBgqCpHh+zYHe2HjtvZopDcP9TyhIH4y9sHlh9K/if7D9mT70pN6b\nFpMM0u3RP3Gjnro+kbueEXvr9uibvBf4EyvlQsxVZ5De9OXeT7WWsP7a9z6RPjPJIJ18a993\nX9i//1My9YG+fy/t0KEPtG/Pe5A+8iPtn59mkFRUQdKYZJDG9uGOf/8nTnWhnDihGDr9xfbP\nzyhIr351gSeNIUgKD9+tmVJdKK96lWapDjMK0v33F3jSmIqC9J5vKFYa7ED/4EK5vfu/OUOR\nH02UQdoX+eXZ9ddHPrivSJD+8GTvp8r0jz+TPlNRkFR/og32rX9j473/rRgqE6RocyBqU3Pg\n6th9mfPPj330as3+7X5PlGs2t7/jVAd6sBejSqr9/cOl7Z+/M9ociHtH+1O9/e3tnydILWYf\npK5mgyHV/nR3FYvYF/2OFXVux4++qvP7lrcohghSOu8X6oEDiiFHQUroKv7OA+1P5fv8EiTF\n0NSbDXH3P2a9j7N54P2aqeHO72yaDXE3fEWx0mDf+r3/jL+xccHHFEPf0Nwqvb2jORJHsyGd\n898jeQ9SV7Mhbri7ngP+6EmzofgaGabebIjz/qOxCs2G4mtkmHqzIa7KINFsSB+i2ZDHe5Bo\nNqSj2RAMGKSn/Z1iSLW/L6q+KN83a+YepFk2G+Ie0gx1NRsMEaR0zoNEs2EMNBvSOQ8SzYYx\n+D6/BEkxRLMhD82GJadBotkQ0GwQNBuKr5HBe5BoNgiaDcXXyECzQXh/jUmzofgaGWg2CO9B\notmQPkSzIY/3INFsSEezIaDZIHzfrJl7kGg2CJoNAUFKR7NhMmg2pHMeJJoNY/B9fgmSYohm\nQx6aDUtOg0SzIaDZIGg2FF8jg/cg0WwQNBuKr5GBZoPw/hqTZkPxNTLQbBDeg0SzIX2IZkMe\n70Gi2ZCOZkNAs0H4vlkz9yDRbBA0GwKClI5mw2TQbEjnPEg0G8bg+/wSJMUQzYY8NBuWnAaJ\nZkNAs0HQbCi+RgbvQaLZIGg2FF8jA80G4f01Js2G4mtkoNkgvAeJZkP6EM2GPN6DRLMhHc2G\ngGaD8H2zZu5BotkgaDYEBCkdzYbJoNmQznmQaDaMwff5JUiKIZoNeWg2LDkNEs2GgGaDoNlQ\nfI0M3oNEs0HQbCi+RgaaDcL7a0yaDcXXyECzQXgPEs2G9CGaDXm8B4lmQzqaDQHNBuH7Zs3c\ng0SzQdBsCAhSOpoNk0GzIZ3zINFsGIPv80uQFEM0G/LQbFhyGiSaDQHNBkGzofgaGbwHiWaD\noNlQfI0MNBuE99eYNBuKr5GBZoPwHiSaDelDNBvyeA8SzYZ0NBsCmg3C982auQeJZoOg2RAQ\npHQ0GyaDZkM650Gi2TAG3+eXICmGaDbkodmw5DRINBsCmg2CZkPxNTJ4DxLNBkGzofgaGWg2\nCO+vMWk2FF8jA80G4T1INBvSh2g25PEeJJoN6Wg2BDQbhO+bNXMPEs0GQbMhIEjpaDZMBs2G\ndM6DRLNhDL7PL0FSDNFsyEOzYclpkGg2BDQbBM2G4mtk8B4kmg2CZkPxNTLQbBDeX2PSbCi+\nRgaaDcJ7kGg2pA/RbMjjPUg0G9LRbAhoNgjfN2vmHiSaDYJmQ0CQ0tFsmAyaDemcB4lmwxh8\nn1+CpBii2ZCHZsOS0yDRbAhoNgiaDcXXyOA9SDQbBM2G4mtkoNkgvL/GpNlQfI0MNBuE9yDR\nbEgfotmQx3uQaDako9kQ0GwQvm/WzD1INBsEzYaAIKWj2TAZNBvSOQ8SzYYx+D6/BEkxRLMh\nD82GJadBotkQ0GwQNBuKr5HBe5BoNgiaDcXXyECzQXh/jUmzofgaGWg2CO9BotmQPkSzIY/3\nINFsiDzD0lk/T7MhoNkgfN+smXuQaDYImg3BlILU/nnnQaLZMAaaDZFnmHaQaDaMwff5HfNH\nu7N/vsog0WwIaDYslXmNdPLaq8XexYPpT0qzIaDZICpvNvx/hhpJ+tbrrxM/7fv3SN6DRLNB\n1NpseNw3orP/cOf8F7I0G4T315izaDZMNkg0G4T3IFXebFhGyDhINBvyeA8SzYbIM0ReI21G\nsyGg2SB836zxc9duM+e3vwdEs0EQpNhTTPr3SDQbxkCzIZ3zINFsGIPv80uQFEM0G/LQbFhy\nGiSaDQHNBlF5s6GL898jeQ8SzQZRa7OhJ+dBotkgvL/GnEWz4eycB4lmg/AepMqbDV1oNgQ0\nGwTNhnQ0GwKaDcL3zZq5B4lmg6DZEBCkdDQbJoNmQzrnQaLZMAbf55cgKYZoNuSh2bDkNEg0\nGwKaDYJmQ/E1MngPEs0GQbOh+BoZaDYI768xaTYUXyMDzQbhPUg0G9KHaDbk8R4kmg3paDYE\nNBuE75s1cw8SzQZBsyEgSOloNkwGzYZ0zoNEs2EMvs8vQVIM0WzIQ7NhyWmQaDYENBsEzYbi\na2TwHiSaDYJmQ/E1MtBsEN5fY9JsKL5GBpoNwnuQaDakD9FsyOM9SDQb0tFsCGg2CN83a+Ye\nJJoNgmZDQJDS0WyYDJoN6ZwHiWbDGHyfX4KkGKLZkIdmw5LTINFsCGg2CJoNxdfI4D1INBsE\nzYbia2Sg2SC8v8ak2VB8jQw0G4T3INFsSB+i2ZDHe5BoNqSj2RDQbBC+b9bMPUg0GwTNhoAg\npaPZMBk0G9I5DxLNhjH4Pr8ESTFEsyEPzYYlp0Gi2RDQbBA0G4qvkcF7kGg2CJoNxdfIQLNB\neH+NSbOh+BoZaDYI70Gi2ZA+RLMhj/cg0WxIR7MhoNkgfN+smXuQaDYImg0BQUpHs2EyaDak\ncx4kmg1j8H1+CZJiiGZDHpoNS06DRLMhoNkgaDYUXyOD9yDRbBA0G4qvkYFmg/D+GpNmQ/E1\nMtBsEN6DRLMhfYhmQx7vQaLZkI5mQ0CzQfi+WTP3INFsEDQbAoKUjmbDZNBsSOc8SDQbxuD7\n/BIkxRDNhjw0G5acBolmQ0CzQdBsKL5GBu9BotkgaDYUXyMDzQbh/TUmzYbia2Sg2SC8B4lm\nQ/oQzYY83oNEsyEdzYaAZoPwfbNm7kGi2SBoNgQEKR3Nhsmg2ZDOeZBoNozB9/klSIohmg15\naDYsOQ0SzYaAZoOg2VB8jQzeg0SzQdBsKL5GBpoNwvtrTJoNxdfIQLNBeA8SzYb0IZoNebwH\niWZDOpoNAc0G4ftmzdyDRLNB0GwICFI6mg2TQbMhnfMg0WwYg+/zS5AUQzQb8tBsWHIaJJoN\nAc0GQbOh+BoZvAeJZoOg2VB8jQw0G4T315g0G4qvkYFmg/AeJJoN6UM0G/J4DxLNhnQ0GwKa\nDcL3zZq5B4lmg6DZEBCkdDQbJoNmQzrnQaLZMAbf55cgKYZoNuSh2bDkNEg0GwKaDYJmQ/E1\nMngPEs0GQbOh+BoZaDYI768xaTYUXyMDzQbhPUg0G9KHaDbk8R4kmg3paDYENBuE75s1cw8S\nzQZBsyEgSOloNkwGzYZ0zoNEs2EMvs8vQVIM0WzIQ7NhyWmQaDYENBsEzYbia2TwHiSaDYJm\nQ/E1MtBsEN5fY9JsKL5GBpoNwnuQaDakD9FsyOM9SDQb0tFsCGg2CN83a+YeJJoNgmZDQJDS\n0WyYDJoN6ZwHiWbDGHyfX4KkGKLZkIdmw5LTINFsCGg2CJoNxdfI4D1INBsEzYbia2Sg2SC8\nv8ak2VB8jQw0G4T3INFsSB+i2ZDHe5BoNqSj2RDQbBC+b9bMPUg0GwTNhoAgpaPZMBk0G9I5\nDxLNhjH4Pr8ESTFEsyEPzYYlp0Gi2RDQbBA0G4qvkcF7kGg2CJoNxdfIQLNBeH+NSbOh+BoZ\naDYI70Gi2ZA+RLMhj/cg0WxIR7MhoNkgfN+smXuQaDYImg0BQUpHs2EyaDakcx4kmg1j8H1+\nCZJiiGZDHpoNS06DRLMhoNkgaDYUXyOD9yDRbBA0G4qvkYFmg/D+GpNmQ/E1MtBsEN6DRLMh\nfYhmQx7vQaLZkI5mQ0CzQfi+WTP3INFsEDQbAoKUjmbDZNBsSOc8SDQbxuD7/BIkxRDNhjw0\nG5acBolmQ0CzQdBsKL5GBu9BotkgaDYUXyMDzQbh/TUmzYbia2Sg2SC8B4lmQ/oQzYY83oNE\nsyEdzYaAZoPwfbNm7kGi2SBoNgQEKR3Nhsmg2ZDOeZBoNozB9/klSIohmg15aDYsOQ0SzYaA\nZoOg2VB8jQzeg0SzQdBsKL5GBpoNwvtrTJoNxdfIQLNBeA8SzYb0IZoNebwHiWZDOpoNAc0G\n4ftmzdyDRLNB0GwICFI6mg2TQbMhnfMg0WwYg+/zS5AUQzQb8iQ0Gz65XzzxKnn4B73naTak\nD9FsCCpqNvz11eLSl8rDN/Sep9lQfI0M3oNEs0HQbCi+RgaaDcL7a0yaDcXXyECzQXgPEs2G\n9CGaDXmqDJIKzYZ0NBuE92bDcLj9nY5mg/DebBgOQUpHs6FuNBvSh6r8GZ9mQx6aDelD3oNE\nsyHQvWeDSrXNhsUi/O+i5WloNmSpqNmQq85mw0Lis1i0Jsn575G8B4lmg6iz2bA5SOtvThHO\ng0SzQXh/jVlts2GZns3/P8Z5kGg2CO9BqrbZUDBINBvyVBkklSk0G84epHu3Lzb5TvpT02zI\nQ7MhmMLt77MH6bFPHhUHF4pfs9NsCGg25HEbpE236Pr9aPcZ30Gi2TAZ1TYbqggSzYbJqLbZ\nMOMg0WwIaDYsGTQbOn6PpAoSzYaAZoOos9mwsSlIrc0GVZCG4z1INBtEnc2Gjb5dO+dBotkg\nXL7GvHPLImLLnSXX9Nn+dh4kmg3CZZBOf0J+kXLrrfLwE6dLrllRkGg25KknSLmm0GzoZ7i7\ndjQbBM2GYAq3v/txfvt7QDQbRkCQ0tFswBkIUjrvFyrNhhEQpHQ0G0SVzYYEV1wUXHiuPLzo\n93pOVxQkmg0BzQaNjxwKbnqzPDz07z2nKwrScLwHiWbD8AiSAs0G4f015mAIkgLNBkGQVioK\nEs2GPAQpR0VBotmQh2ZDjrkHiWaDoNmQgyAphmg2oIkgKYZoNqBp6kE6ep3Y8mJ52HH1nV6/\n89eVV8rDT/b++yo0G/L4bDbkmXqQbrtaPOul8vDX24dy/wal91/I0mwY3tSDNArvQaLZMDyC\npECzQXh/jTkYgqRAs0EQpBWCNBiaDQFBWiJIKjQbApoNSwRpODQbJoIgKdBsQBNBUqDZgCaC\npECzIQ/NhiWC5PwXsjQbhkeQFLwHiWbD8AiSAs0Gwc2QFYKkQLNBEKQVgjQYmg0BQVoiSCo0\nGwKaDUsEaTg0GyaCICnQbEATQVKg2YAmgqRAsyEPzYYlguT8F7I0G4ZHkBS8B4lmw/AIkgLN\nBsHNkBWCpECzQRCkFYJU1s1XiCc8Wx7+cu/50r+Q/c4xseOgPPxq7/mE/d18INi3VR4e+Lxm\n1w4RpLI+vV+85PXy8I97z5cO0v7YW2UuLm4feuTjsXeq/fgj7VPX7g1+8nx5uPfP7L6WUREk\n30o3G06fEt9YP/xe+9BdW2Pp23qXYqvVIEgKNBvQRJAUaDagiSApeL9Qve+vRgRJwfuF6n1/\nNSJICt4vVF2zATkIkoKu2aAyYLMBOQiSgq7ZoDJgswE5CJJv3v/VdawQJN8I0kQQJN8GfM8G\n5CBICqWbDd+/7Zbg8svl4W3f7zvv/a5ijQiSQukL9Z4L9wQ7dsjDC+/pO0+QhkeQFLxfqN73\nVyOCpOD9QvW+vxoRJAXvFyrNhuERJIUBmw0qNBuGR5AUBmw2YCIIEmCAIAEGCFKFaDYMjyAp\nDPieDSre7yrWiCApeL9Qve+vRgRJwfuF6n1/NSJICt4vVO/7qxFBUvB+odJsGB5BUqDZgCaC\npECzAU0ECTBAkAADBKlCNBuGR5AUaDagiSApeL9Qve+vRgRJwfuF6n1/NSJICt4vVO/7qxFB\nUvB5oT4r+s/B/sbY25oJgqTgs9nwz/LPIn/kffLw6MmxtzUTBEmBZgOaCBJggCABBggSYIAg\nKXhvNmB4BEnB5+1vjIkgKRAkNBEkBYKEJoKkQJDQRJAUfDYbMCaCpECzAU0ECTBAkAADBAkw\nQJAUaDagiSApcPsbTQRJgSChiSApECQ0ESQFgoQmgqRAswFNBEmBZgOaCBJggCABBggSYIAg\nKdBsQBNBUuD2N5oIkgJBQhNBUiBIaCJICgQJTQRJgWYDmgiSAs0GNBEkwABBAgwQJMAAQVKg\n2YAmgqTA7W80ESQFgoQmgqRAkNBEkBQIEpoIkgLNBjQRJAWaDWgiSIABggQYIEiAAYKkQLMB\nTQRJgdvfaCJICgQJTQRJgSChiSApECQ0ESQFmg1oIkgKNBvQRJAAAwQJMECQAAMESYFmA5oI\nkgK3v9FEkBQIEpoIkgJBQhNBUiBIaCJICjQb0ESQFGg2oIkgAQYIEmCAIAEGCJICzQY0ESQF\nbn+jiSApECQ0ESQFgoQmgqRAkNBEkHp77p5g1znycM87x94WXCBIvd11NDhySB4e/drY24IL\nBAkwQJAAAwQJMECQAAMECTBAkAADBAkwQJAAAwQJMECQAAMECTBAkAADBAkwQJAAAwQJMECQ\nAAMECTBAkAADBAkwQJAAAwQJMOAzSHcsgIm5I/kyLx+kjePH0m1/883pXvQixdCbtymGbt7G\n/lbc72+74vI7nn6VDxAkjfMOK4b27VMM6d5plf0Fde4vHUGq8kJgfwFBSseFINhfQJDScSEI\n9hcQpHRcCIL9BQQpHReCYH8BQUrHhSDYX0CQ0nEhCPYXEKR0XAiC/QUEKR0XgmB/wcyDtOej\niqHrrlMMfXSPYoj9iTr3l85pkO57VDF06pRi6NH7FEPsT9S5v3ROgwRMC0ECDBAkwABBAgwQ\nJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMeAzSYrEID4ZYaLEovc56rY2kL0q5vwGXGo73\nL8rhcVvIV174ACwXWiyGONKaL0q5vwGXGo77L8rfYfvhF736yhOPWZC40mLTH3eFlwoPy+5v\nqKVUR8L98VPyGST5ptx/SHl2NlLPTtZSqV+Ubn9DLTVwkAY6fkpOg7Sx+oMkcUqxUvqBVi+V\n+kVlBGnIpVI5P35KXoO0sfzxNnVKs1LqeMZSaV+UbtrLAtUAAALGSURBVH/DL5XI+fFT8hek\nTQet9DldbDo/KVOqpcJKaT8FPe5/vS01XJCGPH46foO0kXTMspYa4jirvijV/gZcajjuvyin\nxw2YFoIEGCBIgAHXQcp5WTrAEPvLG3K/v6QVSi+Qw/uBZn9ZQ+73l7RC6QWAOSBIgAGCVJnN\nP8T0/oFmuCH3+9NyGCTpVaX/On+IIef729wtS7zmhhhyvz8tf0EKHV9FGWSIIe/7836het+f\nlscgbayuIVXNvvSQ9/15v1C970/LXZC8H2j2V/f+tHwGafV1px2zQYamsT/HF6r3/WkRpLSh\naezP8YXqfX9aBCltaBr7c3yhet+flrsg/fBrlqsn5fQMNOR9f497S4T+19xQQ+73p+UxSIvw\np0jK1T3UkPf9eb9Qve9Py1+Q5LJJ+uKHG3K/P4yBUwQYIEiAAYIEGHAdJNVrg+GG2F/ekPv9\nJa1QeoEc3g80+8sacr+/pBVKLwDMAUECDLgLkvdf2Hn/cYTjlzWkRpBUF0LiSRpuiOOXN6Tm\nLkjLE+R3yP+FwPHLGFJzGCTfl8IULgSOn3pIzWWQPF8K07gQOH4EacXrpTCVC4HjR5ACzVkt\nPjShC4Hjlz6kRpASh7zfFXv8E/gbmtDxS+I2SH5/NJnGhcDxI0gbfi+DqeD4Dc3lQeAyyMPx\nG57Dw8BlkIfjNwZ3B6LWn6GHwvEbh7sDwIWQh+M3jtkfAMACQQIMECTAAEECDBAkwABBAgwQ\nJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEEC\nDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAA\nQQIMECTAAEECDBAkwABBAgwQJMAAQQIMECTAAEECDBAkwABBAgwQJMAAQQIM/B/ECtOkpDZ5\nxwAAAABJRU5ErkJggg==",
      "text/plain": [
       "plot without title"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "boxplot(log10(assays(dds)[[\"cooks\"]]), range=0, las=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "# interaction factor\n",
    "dds = DESeqDataSetFromMatrix( countData = chloCountTable, colData = chloInfoTable,\n",
    "                              design = ~medium + phase + medium:phase )\n",
    "sizeFactors( dds ) = 2^offsets\n",
    "dds = DESeq( dds, quiet = TRUE, parallel = TRUE )\n",
    "\n",
    "resIT = results(dds, name = 'mediumM.phaseS', parallel = TRUE )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "out of 52770 with nonzero total read count\n",
      "adjusted p-value < 0.1\n",
      "LFC > 0 (up)       : 1359, 2.6%\n",
      "LFC < 0 (down)     : 3795, 7.2%\n",
      "outliers [1]       : 10266, 19%\n",
      "low counts [2]     : 17152, 33%\n",
      "(mean count < 9)\n",
      "[1] see 'cooksCutoff' argument of ?results\n",
      "[2] see 'independentFiltering' argument of ?results\n",
      "\n"
     ]
    }
   ],
   "source": [
    "summary(resIT)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mean of counts for each condition\n",
    "norm_counts = counts(dds, normalized=TRUE)\n",
    "mean_counts = list()\n",
    "for( cond in c('F.E', 'F.S', 'M.E', 'M.S') ){\n",
    "    # list the replicates\n",
    "    reps = paste( cond, c(1,2,3), sep='.')\n",
    "    # make the mean of counts\n",
    "    mean_counts[[cond]] = rowMeans( norm_counts[ , reps ] )\n",
    "}\n",
    "mean_counts = data.frame( mean_counts )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```R\n",
    "# export\n",
    "write.csv( offsets, '../data/dea_results/svcd-offsets.csv' )\n",
    "write.csv( resE, '../data/dea_results/svcd-ME_vs_FE.csv' )\n",
    "write.csv( resS, '../data/dea_results/svcd-MS_vs_FS.csv' )\n",
    "write.csv( resIT, '../data/dea_results/svcd-interaction_term.csv' )\n",
    "write.csv( norm_counts, '../data/dea_results/svcd-normalized_counts.csv' )\n",
    "write.csv( mean_counts, '../data/dea_results/svcd-mean_norm_counts.csv' )\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R [conda env:hs2_r_env]",
   "language": "R",
   "name": "conda-env-hs2_r_env-r"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
